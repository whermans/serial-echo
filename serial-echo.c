#include "/usr/lib/avr/include/stdint.h"
#include <avr/io.h>
#include "inc/serial.h"

#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#include <util/delay.h>

void setup(void);
void loop(void);

int main(void)
{
  setup();
  for(;;)
  {
    loop();
  }
}

void setup(void)
{
  serial_init();
}

void loop(void)
{
  serial_put(serial_get());
}
