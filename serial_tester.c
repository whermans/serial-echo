#include <avr/io.h> 
#include <stdio.h>
#include <stdlib.h>
#include "/usr/lib/avr/include/stdint.h"
 
#ifndef F_CPU
#define F_CPU 16000000UL
#endif
 
#define BAUD 9600
#include <util/setbaud.h>

void setup(void);
void loop();

char serial_get(void);
void serial_put(char c);

int main(void)
{
	setup();
	for(;;)
	{
		loop();
	}
}

void setup(void)
{
  UBRR0H = UBRRH_VALUE;
  UBRR0L = UBRRL_VALUE;
 
#if USE_2X
  UCSR0A |= _BV(U2X0);
#else
  UCSR0A &= ~(_BV(U2X0));
#endif
 
  UCSR0C = _BV(UCSZ01) | _BV(UCSZ00);
  UCSR0B = _BV(RXEN0) | _BV(TXEN0);
}

void loop(void)
{
	char buffer[80];
  int count;
  int i;
  char c;

  count = 0;

  while(count < 79 && (c = serial_get()) != '\n')
  {
    buffer[count++] = c;
  }

  for(i = count; i >= 0; i--)
  {
    serial_put(buffer[i]);
  }

  serial_put('\n');
}
 
char serial_get(void)
{
  char c;
 
  loop_until_bit_is_set(UCSR0A, RXC0);
  c = UDR0;
  return c;
}

void serial_put(char c)
{
  loop_until_bit_is_set(UCSR0A, UDRE0);
  UDR0 = c;
}
