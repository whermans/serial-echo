#ifndef INC_SERIAL
#define INC_SERIAL

void serial_init(void);

void serial_put(char c);
char serial_get(void);
#endif
