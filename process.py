#!/usr/bin/env python3

import sys
from subprocess import call

def main():
    main = ""
    sources = []
    outname = ""
    argc = len(sys.argv)
    argv = sys.argv[1:]

    if(argc > 1):
        main = argv[0]
        print("Main source file: " + main)
    else:
        print("No source file found, exiting")
        exit(1)
    if(argc > 2):
        sources = argv[1:]
        print("Additional source files: " + " ".join(sources))

    if(main.endswith(".c")):
        outname = main[:-2]
        print("Compiling to " + outname + ".elf")
    else:
        print(main + " does not appear to be a C source file")
        exit(1)

    ret = call(["avr-gcc", main, "-o", "build/" + outname + ".elf", "-mmcu=atmega328p", "-Wall", "-Wextra", "-pedantic", "-Os"] + sources)

    if(ret != 0):
        print("Shit's broken, ignoring for now")

    ret = call(["avr-objcopy", "-j", ".text", "-O", "ihex", "build/" + outname + ".elf", "bin/" + outname + ".hex"])

    if(ret != 0):
        print("More broken shit, ignoring")

    call(["avrdude", "-v", "-v", "-v", "-v", "-c", "arduino", "-p", "atmega328p", "-P", "/dev/ttyACM0", "-U", "flash:w:bin/" + outname + ".hex"])

main()
